/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_sockets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Julian Alarcon
 */
public class Conexion {
    Connection con=null;
    public Connection conexion(){
        try {
           Class.forName("com.mysql.jdbc.Driver");
           con=DriverManager.getConnection("jdbc:mysql://localhost/sockets","root","");
           System.out.println("Conexión Exitosa");
           JOptionPane.showMessageDialog(null, "Conexión Exitosa");
           
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error De Conexión");
            JOptionPane.showMessageDialog(null, "Error De Conexión"+e);
        }
        return con;
    }
    
}
